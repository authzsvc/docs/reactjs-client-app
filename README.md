## ReactJS Keycloak client application

This project uses the [keycloak-js-react](https://gitlab.cern.ch/authzsvc/libraries/keycloak-js-react) library maintained by the Authorization Service
team in order to authenticate a user (and refresh his token once it expires).

Steps for setup:
- go to the [application portal](https://application-portal.web.cern.ch/) and register a new **OIDC** application, see the docs [here](https://auth.docs.cern.ch/applications/sso-registration/) for more information.
- change the `REACT_APP_KEYCLOAK_CLIENT_ID` variable to use your project's client_id, in the `.env` file.
- you will be prompted to login and the contents of your token will be displayed on the page.

Before running the project make sure you run `yarn install` in the current folder. If you don't have yarn, you may install it by running `npm i -g yarn`.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
