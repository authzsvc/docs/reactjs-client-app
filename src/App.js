import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  keycloakAuthenticated,
  keycloakUserToken,
  keycloakInstance,
} from "./selectors";
import PropTypes from "prop-types";

const App = ({ authenticated, userToken, kcInstance }) => {
  const logout = () => {
    kcInstance.logout();
  };

  const [userInfo, setUserInfo] = useState({});

  useEffect(() => {
    const getData = async () => {
      const userInfoResponse = await fetch(
        `${userToken.iss}/protocol/openid-connect/userinfo`,
        {
          headers: { Authorization: `Bearer ${kcInstance.token}` },
        }
      );
      setUserInfo(await userInfoResponse.json());
    };
    getData();
  }, [userToken, kcInstance.token]);

  return (
    <div className="App" style={{ margin: "20px" }}>
      {!authenticated && <p>Loading...</p>}
      {authenticated && (
        <>
          <p>Authentication status: {authenticated ? "true" : "false"}</p>
          {authenticated && (
            <p>
              <button onClick={() => logout()}>Logout</button>
            </p>
          )}
          <hr />
          <p>My user token: </p>
          <div>
            <code style={{ whiteSpace: "pre-wrap" }}>
              {JSON.stringify(userToken, null, 2)}
            </code>
          </div>
          <hr />
          <p>My user info: </p>
          <div>
            <code style={{ whiteSpace: "pre-wrap" }}>
              {JSON.stringify(userInfo, null, 2)}
            </code>
          </div>
        </>
      )}
    </div>
  );
};
App.propTypes = {
  authenticated: PropTypes.bool,
  userToken: PropTypes.object,
};

export default connect((state) => ({
  kcInstance: keycloakInstance(state),
  authenticated: keycloakAuthenticated(state),
  userToken: keycloakUserToken(state),
}))(App);
